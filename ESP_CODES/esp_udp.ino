
#include <SoftwareSerial.h>
SoftwareSerial Serial2(2, 15);

void setup() {
  // put your setup code here, to run once:
Serial.begin(115200);
  Serial2.begin(115200);

   Serial2.println("AT");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil('\n');
     Serial.println("Got reponse from Quectel: " + inData);
     inData="";
  }
  delay(10000);
}

void loop() {
  // put your main code here, to run repeatedly:
while (Serial2.available()){
  Serial2.flush();
}
Serial2.println("AT");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil('\n');
     Serial.println("Got reponse from Quectel: " + inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("module connection failed");
    }
     inData="";
  }
  delay(10000);
  Serial2.println("AT+CPIN?");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("READY")>0)
    {
      break;
    }
    else
    {
      Serial.println("Check your network connection");
    }
     inData="";
  }
  delay(10000);
  Serial2.println("AT+CREG?");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("0,5")>0)
    {
      break;
    }
    else
    {
      Serial.println("failed to register network connection");
    }
     inData="";
  }
  delay(10000);
  Serial2.println("AT+CGREG?");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("0,5")>0)
    {
      break;
    }
    else
    {
      Serial.println("failed to register GPRS network connection");
    }
     inData="";
  }
  delay(10000);
  Serial2.println("AT+QIMODE=0");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("mode failed");
    }
     inData="";
  }
  delay(10000);
   Serial2.println("AT+QICSGP=1,\"CMNET\"");   //write your APN here
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("APN setting failed");
    }
     inData="";
  }
  delay(10000);
   Serial2.println("AT+QIREGAPP");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("UDP  registration failed");
    }
     inData="";
  }
  delay(10000);
   Serial2.println("AT+QICSGP?");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("1")>0)
    {
      break;
    }
    else
    {
      Serial.println("Its not in GPRS connecting mode");
    }
     inData="";
  }
  delay(10000);
    Serial2.println("AT+QIACT");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("GPRS activate failed");
    }
     inData="";
  }
  delay(10000);
   Serial2.println("AT+QILOCIP");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("ERROR")>0)
    {
      Serial.println("Getting IP failed");
    }
    else
    {
      break;
    }
     inData="";
  }
  delay(10000);
     Serial2.println("ATV1");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("setting response failed");
    }
     inData="";
  }
  delay(10000);
    Serial2.println("AT+QIHEAD=1");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("setting header information failed");
    }
     inData="";
  }
  delay(10000);
   Serial2.println("AT+QIDNSIP=0");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("UDP session establishment failed");
    }
     inData="";
  }
  delay(10000);
  Serial2.println("AT+QIOPEN=\"UDP\",\"35.200.248.63\",\"6789\"");     //place the IP and Port number here
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("UDP connection failed");
    }
     inData="";
  }
  delay(3000);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("FAIL")>0)
    {
      break;
    }
    else
    {
      Serial.println("UDP connection failed");
    }
     inData="";
  }
  delay(10000);

   Serial2.println("AT+QISEND");
  delay(300);
  while (Serial2.read()=='>');
{
Serial2.print("hii");  //message to be sent
//Serial2.print("fghij");
delay(500);
Serial2.write(0x1A);  // sends ctrl+z end of message
    Serial2.write(0x0D);  // Carriage Return in Hex
Serial2.write(0x0A);  // Line feed in Hex
//inData="";
//The 0D0A pair of characters is the signal for the end of a line and beginning of another.
//delay(5000);
}
delay(300);
while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("message send failed");
    }
     inData="";
  }

  delay(10000);
 Serial2.println("ATE1");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println(" failed to enable echo mode");
    }
     inData="";
  }
  delay(10000);

   Serial2.println("AT+QIDEACT");
  delay(30);
  while (Serial2.available()){
     String inData = Serial2.readStringUntil("\n");
     Serial.println(inData);
     if(inData.indexOf("OK")>0)
    {
      break;
    }
    else
    {
      Serial.println("deactivate gprs failed");
    }
     inData="";
  }
  delay(10000);
}
