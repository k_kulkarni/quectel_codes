import serial
import RPi.GPIO as GPIO     
import os, time

GPIO.setmode(GPIO.BOARD)   

# Enable Serial Communication
port = serial.Serial("/dev/ttyS0", baudrate=115200, timeout=1)

# Transmitting AT Commands to the Modem
# '\r\n' indicates the Enter key

port.write('AT'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(1)


port.write('ATDXXXXXXXX'+'\r\n')    //place your mobile number here
rcv = port.read(10)
print rcv
time.sleep(15)


port.write('ATH'+'\r\n')        //for hanging up the call
rcv = port.read(1)
print rcv
time.sleep(1)