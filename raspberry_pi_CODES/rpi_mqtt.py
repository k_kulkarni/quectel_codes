import serial
import RPi.GPIO as GPIO     
import os, time
import random
GPIO.setmode(GPIO.BOARD)   

# Enable Serial Communication
port = serial.Serial("/dev/ttyS0", baudrate=115200, timeout=1)

# Transmitting AT Commands to the Modem
# '\r\n' indicates the Enter key

port.write('AT'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(1)



port.write('AT+CGACT=1,1'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)




port.write('AT+CGPADDR=1'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)




port.write('AT+CGATT=1'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)


port.write('AT+QMTCFG=\"KEEPALIVE\",0,120'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(1)



port.write('AT+QMTOPEN?'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)




port.write('AT+QMTOPEN=0,\"iot.eclipse.org\",1883'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)




port.write('AT+QMTCONN=0,'+str(random.randint()(200,1000))+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+QMTPUB=0,0,0,0,\"XXXXX\",5'+'\r\n')              //write your publish topic and length 5-length                        
while(port.read(10)=='>'):
    port.write('hello')                 //the data which you wanted to write
    time.sleep(0.3)
    port.write(0x1A);  // sends ctrl+z end of message
    port.write(0x0D);  // Carriage Return in Hex
    port.write(0x0A);  // Line feed in Hex

time.sleep(3)




port.write('AT+QMTSUB=0,1,\"XXXXXX\",0'+'\r\n')           write your subscribe topic     
rcv = port.read(10)
print rcv
time.sleep(3)


port.write('AT+CGATT=0'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)

