import serial
import RPi.GPIO as GPIO     
import os, time
from asyncore import write

GPIO.setmode(GPIO.BOARD)   

# Enable Serial Communication
port = serial.Serial("/dev/ttyS0", baudrate=115200, timeout=1)

# Transmitting AT Commands to the Modem
# '\r\n' indicates the Enter key

port.write('AT'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)


port.write('AT+CPIN?'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+CREG?'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+CGREG?'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+QIMODE=0'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+QICSGP=1,\"CMNET\"'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)






port.write('AT+QIREGAPP'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+QICSGP?'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+QIACT'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+QILOCIP'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('ATV1'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+QIHEAD=1'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+QIDNSIP=0'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+QIOPEN=\"UDP\",\"XXXXXXXXXXXXXX\",\"XXXX\"'+'\r\n')      //write your ip and port number
rcv = port.read(10)
print rcv
time.sleep(3)


port.write('AT+QISEND'+'\r\n')
# rcv = port.read(10)
# print rcv
while(port.read(10)=='>'):
    port.write('hello')                 //the data which you wanted to write
    time.sleep(0.3)
    port.write(0x1A);  // sends ctrl+z end of message
    port.write(0x0D);  // Carriage Return in Hex
    port.write(0x0A);  // Line feed in Hex

time.sleep(3)



port.write('ATE1'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)



port.write('AT+QIDEACT'+'\r\n')
rcv = port.read(10)
print rcv
time.sleep(3)




